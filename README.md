# BowTiedJungle Books

## Books on finances

- The trifecta by BowTiedBull
  - Efficiency - Get Rich Without Giving Up Your Life
  - Triangle Investing
  - Spending for Maximum Return
- Incerto series by Nassim Taleb

## Books on time management/productivity

- Atomic Habits by James Clear

## Books on health and body 

## Books on psychology

- Influence: The Psychology of Persuasion by Robert Cialdini

## Books on mental health/spirituality

- Kapil Gupta books (A Master's Secret Whispers, Atmamun, Direct Truth)
- Meditations by Marcus Aurelius

## Fiction


